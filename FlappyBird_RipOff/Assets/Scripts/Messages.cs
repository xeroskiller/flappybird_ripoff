﻿using UnityEngine;
using System.Collections;

public static class Messages 
{

    public static string PLAYER_DEATH = "PLAYER_DEATH",
        PLAYER_SCORE = "PLAYER_SCORE",
        START_GAME = "START_GAME";
}
