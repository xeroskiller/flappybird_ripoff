﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {

    // Pipe properties
    public float pipeXPos, 
                 pipeYmin, 
                 pipeYmax, 
                 timeBetweenPipesStart, 
                 timeBetweenPipesScale;

    // Necessary objects
    [SerializeField]
    private GameObject pipePrefab;
    [SerializeField]
    private GameObject pipeContainer;
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Text restartText;
    [SerializeField]
    private Text highScoreText;

    // Internal tracking vars
    private float _timeOfLastPipe, _timeUntilNextPipe;
    private bool _makePipes, _playerDead, _waitingForRestart;
    private int score, highScore;

	// Set scene and create listeners
	void Start () {
        RestartLevel();
        Messenger.AddListener(Messages.PLAYER_DEATH, PlayerDied);
        Messenger.AddListener(Messages.PLAYER_SCORE, PlayerScore);
	}

    public void PlayerDied()
    {
        if (score > highScore)
        {
            highScore = score;
            Congratulate();
        }

        _playerDead = true;
        _makePipes = false;
        StartCoroutine(Restart());
    }

    void Congratulate()
    {
        highScoreText.text = "High: " + highScore.ToString();
    }

    public void PlayerScore()
    {
        score++;

        if (score % 5 == 0)
        {
            Debug.Log("SpeedUp");
            _timeUntilNextPipe *= timeBetweenPipesScale;
        }

        UpdateScoreGUI();
    }

    void UpdateScoreGUI()
    {
        scoreText.text = "Score: " + score.ToString();
    }
	
	// Update is called once per frame
	void Update () {
        // If the player is dead, watch for restart timing and init
        if (_playerDead)
        {
            if (!_waitingForRestart)
                return;
            else if (Input.GetMouseButtonDown(1))
                RestartLevel();
        }

        // Continue making pipes, if time is right
        else if (_makePipes)
        {
            if (Time.time >= _timeOfLastPipe + _timeUntilNextPipe)
                MakePipe();
        }

        // Start pipe generation
        else if (Input.GetMouseButtonDown(0))
        {
            Messenger.Broadcast(Messages.START_GAME);
            _makePipes = true;
            MakePipe();
        }
	}

    private void RestartLevel()
    {
        restartText.gameObject.SetActive(false);
        player.transform.position = new Vector3(player.transform.position.x, 2.0f, -1.0f);
        _playerDead = _waitingForRestart = false;
        player.gameObject.SetActive(true);
        _timeUntilNextPipe = timeBetweenPipesStart;
        score = 0;
    }

    IEnumerator Restart()
    {
        yield return new WaitForSeconds(5);
        restartText.gameObject.SetActive(true);
        _waitingForRestart = true;
        yield return null;
    }

    void MakePipe()
    {
        var p = Instantiate(pipePrefab, new Vector3(pipeXPos, Random.Range(pipeYmin, pipeYmax), -0.5f), Quaternion.identity) as GameObject;
        p.transform.parent = pipeContainer.transform;
        _timeOfLastPipe = Time.time;
    }
}
