﻿using UnityEngine;
using System.Collections;

public class PipeMover : MonoBehaviour {

    public float speed;
    public float destroyAtX;
	
	// Update is called once per frame
	void Update () {
        transform.Translate(new Vector3(-speed * Time.deltaTime, 0.0f, 0.0f));

        if (transform.position.x <= destroyAtX)
        {
            Destroy(gameObject);
        }
	}


}
