﻿using UnityEngine;
using System.Collections;

public class PlayerMover : MonoBehaviour {

    public float flapStrength = 6.0f;
    public int maxFlapFrequency = 3;
    public float gravityStrength = 1.5f;
    public float fallDeathYValue;
    public GameObject explosion;

    private Rigidbody2D rb;
    private float _lastFlap;
    private float _minTimeBetweenFlaps;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        _minTimeBetweenFlaps = 1.0f / maxFlapFrequency;
        _lastFlap = 0.0f;
        // TurnOffGravity();

        Messenger.AddListener(Messages.START_GAME, TurnOnGravity);
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetMouseButtonDown(0) && Time.time >= _lastFlap + _minTimeBetweenFlaps)
            Flap();

        if (transform.position.y <= fallDeathYValue)
            KillPlayer();
	}

    public void TurnOnGravity()
    {
        rb.gravityScale = gravityStrength;
    }

    public void TurnOffGravity()
    {
        rb.gravityScale = 0.0f;
        rb.velocity = new Vector2(0f, 0f);
    }

    public void Flap()
    {
        float yVel = rb.velocity.y;

        rb.AddForce(new Vector2(0.0f, flapStrength + (yVel < 0 ? Mathf.Abs(yVel) : 0.0f)), ForceMode2D.Impulse);
        _lastFlap = Time.time;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.tag.Equals("Score"))
            KillPlayer();
        else
            Messenger.Broadcast(Messages.PLAYER_SCORE);
    }

    void KillPlayer()
    {
        TurnOffGravity();
        Messenger.Broadcast(Messages.PLAYER_DEATH);
        StartCoroutine(ExplosionMaker(transform.position));
        gameObject.SetActive(false);
        
    }

    IEnumerator ExplosionMaker(Vector3 pos)
    {
        var p = Instantiate(explosion, pos, Quaternion.identity);
        yield return new WaitForSeconds(5);
        Destroy(p);
        yield return null;
    }
}
